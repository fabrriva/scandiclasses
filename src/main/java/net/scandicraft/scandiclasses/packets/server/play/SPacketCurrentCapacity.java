package net.scandicraft.scandiclasses.packets.server.play;

import net.minecraft.server.v1_8_R3.PacketDataSerializer;
import net.scandicraft.scandiclasses.capacities.ICapacity;
import net.scandicraft.scandiclasses.packets.server.SPacket;

import java.io.IOException;

/**
 * Packet qui informe le client de la capacité actuelle
 */
public class SPacketCurrentCapacity extends SPacket {
    private ICapacity currentCapactiy = null;

    public SPacketCurrentCapacity() {
    }

    public SPacketCurrentCapacity(ICapacity currentCapacity) {
        this.currentCapactiy = currentCapacity;
    }

    @Override
    public void writePacketData(PacketDataSerializer data) throws IOException {
        writeString(data, this.currentCapactiy.getUniqueIdentifier());
    }
}
