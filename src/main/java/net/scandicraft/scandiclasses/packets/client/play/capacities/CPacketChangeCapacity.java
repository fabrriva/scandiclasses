package net.scandicraft.scandiclasses.packets.client.play.capacities;

import net.minecraft.server.v1_8_R3.PacketDataSerializer;
import net.scandicraft.scandiclasses.capacities.CapacitiesAction;
import net.scandicraft.scandiclasses.capacities.CapacityManager;
import net.scandicraft.scandiclasses.logs.LogManager;
import org.bukkit.entity.Player;

import java.io.IOException;

public class CPacketChangeCapacity extends CPacketCapacities {
    @Override
    public void readPacketData(PacketDataSerializer data) throws IOException {
        CapacitiesAction action = readEnum(data, CapacitiesAction.class);
        LogManager.consoleWarn("CPacketChangeCapacity readPacketData action " + action);
        switch (action) {
            case CHANGE_CURRENT_CAPACITY:
                LogManager.consoleWarn("CPacketChangeCapacity case CHANGE_CURRENT_CAPACITY");
                this.onPacketHandle = (playerConnection) -> {
                    LogManager.consoleWarn("CPacketChangeCapacity onPacketHandle");
                    Player player = playerConnection.getPlayer();
                    LogManager.consoleWarn("CPacketChangeCapacity onPacketHandle player " + player);
                    String nextCapacity = readString(data);
                    LogManager.consoleWarn("CPacketChangeCapacity onPacketHandle string " + nextCapacity);
                    LogManager.consoleWarn("onPacketHandle CHANGE_CURRENT_CAPACITY " + player + " - " + nextCapacity);
                    if (nextCapacity.equals("next_from_list")) {
                        CapacityManager.getInstance().selectNextCapacity(player);
                    } else {
                        LogManager.consoleWarn(this.getClass().getSimpleName() + " selection specific capacity " + nextCapacity);
//                CapacityManager.getInstance().changeCurrentCapacity(player, null);
                    }
                };
                break;
        }
    }
}
