package net.scandicraft.scandiclasses.packets.client.play.capacities;

import net.minecraft.server.v1_8_R3.PacketDataSerializer;
import net.minecraft.server.v1_8_R3.PacketListener;
import net.minecraft.server.v1_8_R3.PlayerConnection;
import net.scandicraft.scandiclasses.logs.LogManager;
import net.scandicraft.scandiclasses.packets.Callback;
import net.scandicraft.scandiclasses.packets.client.CPacket;

import java.io.IOException;

public abstract class CPacketCapacities extends CPacket {
    protected Callback<PlayerConnection> onPacketHandle;

    public abstract void readPacketData(PacketDataSerializer data) throws IOException;

    @Override
    public void handle(PacketListener listener) {
        LogManager.consoleWarn("CPacketCapacities handle");
        if (this.onPacketHandle != null) {
            PlayerConnection playerConnection = (PlayerConnection) listener;
            this.onPacketHandle.call(playerConnection);
        }
    }
}
