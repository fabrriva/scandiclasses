package net.scandicraft.scandiclasses.packets;

public interface Callback<T> {
    void call(T arg);
}