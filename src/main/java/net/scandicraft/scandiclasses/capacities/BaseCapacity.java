package net.scandicraft.scandiclasses.capacities;

import net.scandicraft.scandiclasses.capacities.exception.CapacityException;
import net.scandicraft.scandiclasses.capacities.utils.CapacityUtils;
import net.scandicraft.scandiclasses.config.CapacitiesConfig;
import net.scandicraft.scandiclasses.packets.Callback;
import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;

import java.util.concurrent.TimeUnit;

public abstract class BaseCapacity implements ICapacity {

    @Override
    public void sendSucessMessage(Player sender) {
        sender.sendMessage(ChatColor.GREEN + "Capacité " + ChatColor.BOLD + this.getName() + ChatColor.RESET + ChatColor.GREEN + " utilisée avec succès.");
    }

    @Override
    public void sendWaitingMessage(Player sender, long cooldown) {
        sender.sendMessage(String.format("%sVous devez attendre %s pour utiliser la capacité %s.", ChatColor.RED, this.getWaitingTime(cooldown), this.getName()));
    }

    private String getWaitingTime(long cooldown) {
        StringBuilder timeWaiting = new StringBuilder();

        long minutes = TimeUnit.SECONDS.toMinutes(cooldown);
        long secondes = TimeUnit.SECONDS.toSeconds(cooldown);

        if (minutes > 0) {
            timeWaiting.append(String.format("%d %s", (int) minutes, minutes <= 1 ? "minute " : "minutes "));
            secondes = cooldown % 60;
        }

        timeWaiting.append(String.format("%d %s", (int) secondes, secondes <= 1 ? "seconde" : "secondes"));

        return timeWaiting.toString();
    }

    protected void targetEntityHandler(Player sender, Callback<LivingEntity> callback) throws CapacityException {
        this.targetHandler(sender, callback, LivingEntity.class, CapacityException.NO_TARGET_ENTITY);
    }

    protected void targetMonsterHandler(Player sender, Callback<Monster> callback) throws CapacityException {
        this.targetHandler(sender, callback, Monster.class, CapacityException.NO_TARGET_MONSTER);
    }

    protected void targetPlayerHandler(Player sender, Callback<Player> callback) throws CapacityException {
        this.targetHandler(sender, callback, Player.class, CapacityException.NO_TARGET_PLAYER);
    }

    private void targetHandler(final Player sender, final Callback callback, Class<? extends Entity> referClass, String no_target_message) throws CapacityException {
        Entity target = CapacityUtils.getTargetEntityClass(sender, CapacitiesConfig.MAX_TARGET_DISTANCE, referClass);
        if (target != null) {
            callback.call(target);

            if (target instanceof Player) {
                target.sendMessage(String.format("%s %s a utilisé la capacité %s sur vous.", ChatColor.RED, sender.getDisplayName(), this.getName()));
            }
        } else {
            throw new CapacityException(no_target_message);
        }
    }

}
