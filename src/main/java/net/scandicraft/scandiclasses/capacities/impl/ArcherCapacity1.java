package net.scandicraft.scandiclasses.capacities.impl;

import net.scandicraft.scandiclasses.capacities.BaseCapacity;
import net.scandicraft.scandiclasses.config.ClassesConfig;
import net.scandicraft.scandiclasses.utils.MathUtils;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

/**
 * tire 3 flèches spectrales devant lui
 */
public class ArcherCapacity1 extends BaseCapacity {
    @Override
    public String getName() {
        return "flèche sanglante";
    }

    @Override
    public int getCooldownTime() {
        return ClassesConfig.COOLDOWN_CAPACITY_1;
    }

    @Override
    public String getUniqueIdentifier() {
        return "ArcherCapacity1";
    }

    @Override
    public void onUse(Player sender) {
        this.shootArrow(sender, 15); //en face
    }

    private void shootArrow(final Player sender, final int nbr_arrow) {
        Vector velocity = sender.getEyeLocation().getDirection();
        double speed = 2F;
        Vector direction = new Vector(velocity.getX() / speed, velocity.getY() / speed, velocity.getZ() / speed);
        double spray = 5.0D;

        //Flèche droit devant
        Arrow front_arrow = sender.getWorld().spawnArrow(sender.getEyeLocation(), new Vector(0, 0, 0), 0.6F, 12F);
        front_arrow.setShooter(sender);
        front_arrow.setVelocity(sender.getEyeLocation().getDirection().multiply(3F));
        front_arrow.setFireTicks(MathUtils.convertSecondsToTicks(10));
        front_arrow.setCritical(true);

        //Pluie de flèche
        for (int i = 0; i < nbr_arrow; i++) {
            Arrow arrow = sender.getWorld().spawnArrow(sender.getEyeLocation(), new Vector(0, 0, 0), 0.6F, 12F);
            arrow.setShooter(sender);

            arrow.setVelocity(new Vector(direction.getX() + (Math.random() - 0.5) / spray, direction.getY() + (Math.random() - 0.5) / spray, direction.getZ() + (Math.random() - 0.5) / spray).normalize().multiply(speed));

            arrow.setCritical(true);
            arrow.setFireTicks(MathUtils.convertSecondsToTicks(5));
        }
    }
}
