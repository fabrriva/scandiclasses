package net.scandicraft.scandiclasses.capacities.impl;

import net.scandicraft.scandiclasses.capacities.BaseCapacity;
import net.scandicraft.scandiclasses.capacities.exception.CapacityException;
import net.scandicraft.scandiclasses.config.ClassesConfig;
import org.bukkit.entity.Player;

/**
 * soigne lui + la personne qui vise
 */
public class MagicienCapacity2 extends BaseCapacity {
    @Override
    public String getName() {
        return "soigneur";
    }

    @Override
    public int getCooldownTime() {
        return ClassesConfig.COOLDOWN_CAPACITY_2;
    }

    @Override
    public String getUniqueIdentifier() {
        return "MagicienCapacity2";
    }

    @Override
    public void onUse(Player sender) throws CapacityException {
        sender.setHealth(sender.getMaxHealth());

        this.targetPlayerHandler(sender, (target) -> {
            target.setHealth(target.getMaxHealth());
        });
    }
}
