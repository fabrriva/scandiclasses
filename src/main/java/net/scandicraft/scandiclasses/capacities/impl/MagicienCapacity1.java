package net.scandicraft.scandiclasses.capacities.impl;

import net.scandicraft.scandiclasses.capacities.BaseCapacity;
import net.scandicraft.scandiclasses.capacities.exception.CapacityException;
import net.scandicraft.scandiclasses.config.ClassesConfig;
import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 * éclair sur la personne ou le bloc qu’il vise
 */
public class MagicienCapacity1 extends BaseCapacity {
    @Override
    public String getName() {
        return "éclair";
    }

    @Override
    public int getCooldownTime() {
        return ClassesConfig.COOLDOWN_CAPACITY_1;
    }

    @Override
    public String getUniqueIdentifier() {
        return "MagicienCapacity1";
    }

    @Override
    public void onUse(Player sender) throws CapacityException {
        this.targetEntityHandler(sender, (target) -> {
            Location targetLightningLocation = target.getLocation();

            if (targetLightningLocation != null) {
                sender.getWorld().strikeLightning(targetLightningLocation);
            }
        });
    }
}
