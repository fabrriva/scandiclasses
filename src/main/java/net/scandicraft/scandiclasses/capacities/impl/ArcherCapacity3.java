package net.scandicraft.scandiclasses.capacities.impl;

import net.scandicraft.scandiclasses.capacities.BaseCapacity;
import net.scandicraft.scandiclasses.capacities.exception.CapacityException;
import net.scandicraft.scandiclasses.config.ClassesConfig;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

/**
 * flèche qui enlève la moitié de la vie actuelle du joueur
 */
public class ArcherCapacity3 extends BaseCapacity {
    @Override
    public String getName() {
        return "flèche piquante";
    }

    @Override
    public int getCooldownTime() {
        return ClassesConfig.COOLDOWN_CAPACITY_3;
    }

    @Override
    public String getUniqueIdentifier() {
        return "ArcherCapacity3";
    }

    @Override
    public void onUse(Player sender) throws CapacityException {
        this.targetEntityHandler(sender, (target) -> {
            Arrow arrow = sender.getWorld().spawnArrow(sender.getEyeLocation(), new Vector(0, 0, 0), 0.6F, 12F);
            arrow.setShooter(sender);
            arrow.setVelocity(sender.getEyeLocation().getDirection().multiply(3F));

            target.setHealth(target.getHealth() / 2);
        });
    }


}
