package net.scandicraft.scandiclasses.capacities.impl;

import net.scandicraft.scandiclasses.capacities.BaseCapacity;
import net.scandicraft.scandiclasses.capacities.exception.CapacityException;
import net.scandicraft.scandiclasses.config.ClassesConfig;
import net.scandicraft.scandiclasses.logs.LogManager;
import org.bukkit.entity.Player;

/**
 * téléporte lui et le joueur visé dans une arène PvP pendant 1 minute
 */
public class MagicienCapacity3 extends BaseCapacity {
    @Override
    public String getName() {
        return "combattant";
    }

    @Override
    public int getCooldownTime() {
        return ClassesConfig.COOLDOWN_CAPACITY_3;
    }

    @Override
    public String getUniqueIdentifier() {
        return "MagicienCapacity3";
    }

    @Override
    public void onUse(Player sender) throws CapacityException {
        this.targetPlayerHandler(sender, (target) -> {
            LogManager.consoleWarn(getName() + " a implémenter..");
        });
    }
}
