package net.scandicraft.scandiclasses.capacities.impl;

import net.scandicraft.scandiclasses.capacities.BaseCapacity;
import net.scandicraft.scandiclasses.capacities.exception.CapacityException;
import net.scandicraft.scandiclasses.config.ClassesConfig;
import org.bukkit.Effect;
import org.bukkit.entity.Player;

/**
 * se téléporte sur le joueur visé
 */
public class GuerrierCapacity2 extends BaseCapacity {
    @Override
    public String getName() {
        return "téléportation guerrière";
    }

    @Override
    public int getCooldownTime() {
        return ClassesConfig.COOLDOWN_CAPACITY_2;
    }

    @Override
    public String getUniqueIdentifier() {
        return "GuerrierCapacity2";
    }

    @Override
    public void onUse(Player sender) throws CapacityException {
        this.targetEntityHandler(sender, (target) -> {
            sender.teleport(target.getLocation());
            sender.getWorld().playEffect(sender.getLocation(), Effect.ENDER_SIGNAL, 50);
        });
    }
}
