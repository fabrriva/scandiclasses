package net.scandicraft.scandiclasses.capacities.exception;

import org.bukkit.ChatColor;

public class CapacityException extends Exception {

    private String playerErrorMessage = "";

    public CapacityException(String playerErrorMessage) {
        super(playerErrorMessage);

        this.playerErrorMessage = playerErrorMessage;
    }

    /**
     * @return erreur que l'on peut afficher au joueur
     */
    public String getPlayerErrorMessage() {
        return ChatColor.RED + playerErrorMessage;
    }

    public static String NO_TARGET_PLAYER = "Aucun joueur visé.";
    public static String NO_TARGET_MONSTER = "Aucun monstre visé.";
    public static String NO_TARGET_ENTITY = "Aucune entité visée.";
}
