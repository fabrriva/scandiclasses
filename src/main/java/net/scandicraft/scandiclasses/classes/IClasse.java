package net.scandicraft.scandiclasses.classes;

import net.scandicraft.scandiclasses.capacities.ICapacity;

import java.util.ArrayList;

public interface IClasse {

    /**
     * @return le type de classe
     */
    ClasseType getClassType();

    /**
     * @return nom de la classe
     */
    String getDisplayClasseName();

    /**
     * Les capacités liées à la classe
     */
    ArrayList<ICapacity> getCapacities();

}
