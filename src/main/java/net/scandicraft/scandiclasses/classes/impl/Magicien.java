package net.scandicraft.scandiclasses.classes.impl;

import net.scandicraft.scandiclasses.capacities.ICapacity;
import net.scandicraft.scandiclasses.capacities.impl.MagicienCapacity1;
import net.scandicraft.scandiclasses.capacities.impl.MagicienCapacity2;
import net.scandicraft.scandiclasses.capacities.impl.MagicienCapacity3;
import net.scandicraft.scandiclasses.classes.ClasseType;
import net.scandicraft.scandiclasses.classes.IClasse;

import java.util.ArrayList;
import java.util.Arrays;

public class Magicien implements IClasse {
    private final ArrayList<ICapacity> capacities = new ArrayList<>(Arrays.asList(
            new MagicienCapacity1(),
            new MagicienCapacity2(),
            new MagicienCapacity3()
    ));

    @Override
    public ClasseType getClassType() {
        return ClasseType.MAGICIEN;
    }

    @Override
    public String getDisplayClasseName() {
        return getClassType().getName();
    }

    @Override
    public ArrayList<ICapacity> getCapacities() {
        return capacities;
    }
}
