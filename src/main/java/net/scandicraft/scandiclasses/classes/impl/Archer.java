package net.scandicraft.scandiclasses.classes.impl;

import net.scandicraft.scandiclasses.capacities.ICapacity;
import net.scandicraft.scandiclasses.capacities.impl.ArcherCapacity1;
import net.scandicraft.scandiclasses.capacities.impl.ArcherCapacity2;
import net.scandicraft.scandiclasses.capacities.impl.ArcherCapacity3;
import net.scandicraft.scandiclasses.classes.ClasseType;
import net.scandicraft.scandiclasses.classes.IClasse;

import java.util.ArrayList;
import java.util.Arrays;

public class Archer implements IClasse {
    private final ArrayList<ICapacity> capacities = new ArrayList<>(Arrays.asList(
            new ArcherCapacity1(),
            new ArcherCapacity2(),
            new ArcherCapacity3()
    ));

    @Override
    public ClasseType getClassType() {
        return ClasseType.ARCHER;
    }

    @Override
    public String getDisplayClasseName() {
        return getClassType().getName();
    }

    @Override
    public ArrayList<ICapacity> getCapacities() {
        return capacities;
    }
}
