package net.scandicraft.scandiclasses.classes.impl;

import net.scandicraft.scandiclasses.capacities.ICapacity;
import net.scandicraft.scandiclasses.capacities.impl.GuerrierCapacity1;
import net.scandicraft.scandiclasses.capacities.impl.GuerrierCapacity2;
import net.scandicraft.scandiclasses.capacities.impl.GuerrierCapacity3;
import net.scandicraft.scandiclasses.classes.ClasseType;
import net.scandicraft.scandiclasses.classes.IClasse;

import java.util.ArrayList;
import java.util.Arrays;

public class Guerrier implements IClasse {
    private final ArrayList<ICapacity> capacities = new ArrayList<>(Arrays.asList(
            new GuerrierCapacity1(),
            new GuerrierCapacity2(),
            new GuerrierCapacity3()
    ));

    @Override
    public ClasseType getClassType() {
        return ClasseType.GUERRIER;
    }

    @Override
    public String getDisplayClasseName() {
        return getClassType().getName();
    }

    @Override
    public ArrayList<ICapacity> getCapacities() {
        return capacities;
    }
}
