package net.scandicraft.scandiclasses.config;

/**
 * Configuration des capacités
 */
public class CapacitiesConfig {

    public static final int MAX_TARGET_DISTANCE = 20; //20 blocs
    public static final boolean HAS_COOLDOWN = false;

}
